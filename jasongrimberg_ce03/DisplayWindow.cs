﻿using System;
using System.Windows.Forms;

namespace JasonGrimberg_CE03
{
    public partial class DisplayWindow : Form
    {
        // -----------------------------------------------------------------------------
        // EventHandler call
        // -----------------------------------------------------------------------------
        public EventHandler SelectionChanged;
        public EventHandler ObjectClear;

        // -----------------------------------------------------------------------------
        // Student data components
        // -----------------------------------------------------------------------------
        // New Students selected object
        public Students SelectedObject
        {
            // Get what the user has selected
            get
            {
                // Make sure that the user has selected an object
                if (lbDisplayWindow.SelectedItems.Count > 0)
                {
                    // Only return the first selected object
                    return lbDisplayWindow.SelectedItems[0].Tag as Students;
                }
                else
                {
                    // Return nothing when the user has not selected anything
                    return new Students();
                }
            }
        }

        // -----------------------------------------------------------------------------
        // Initialize MainForm
        // -----------------------------------------------------------------------------
        public DisplayWindow()
        {
            InitializeComponent();
        }
        
        // -----------------------------------------------------------------------------
        // Object Handlers
        // -----------------------------------------------------------------------------
        // Add new object event handler
        public void ObjectAddedHandler(object sender, EventArgs e)
        {
            // Set the main form as the sender
            MainForm main = sender as MainForm;

            // Set the Student Data as the main form student data
            Students s = main.Data;

            // Create a new list to put objects into
            ListViewItem lvi = new ListViewItem();

            // Set the new list items as strings
            lvi.Text = s.ToString();

            // Set the tag as the student data
            lvi.Tag = s;

            // Add the new object to the list
            lbDisplayWindow.Items.Add(lvi);
        }

        // Clear all event handler
        public void ClearAllHandler(object sender, EventArgs e)
        {

        }

        // -----------------------------------------------------------------------------
        // Change Events
        // -----------------------------------------------------------------------------
        // Selection changed event
        private void lbDisplayWindow_SelectedIndexChanged(object sender, EventArgs e)
        {
            // Invoke the selection to the current selection
            SelectionChanged?.Invoke(this, new EventArgs());
        }

        // -----------------------------------------------------------------------------
        // Triggers
        // -----------------------------------------------------------------------------
        // Second form load trigger
        private void DisplayWindow_Load(object sender, EventArgs e)
        {
            // Set a new main form
            MainForm mainForm = new MainForm();

            // Sub to the Object clear event handler
            ObjectClear += mainForm.ObjectClearHandler;
        }

        // Close out of the application when the second form is closed
        private void DisplayWindow_FormClosed(object sender, FormClosedEventArgs e)
        {
            // Exit the application
            Application.Exit();
        }

        // Clear all event button
        private void tsClear_Click(object sender, EventArgs e)
        {
            lbDisplayWindow.Clear(); 

            // Object clear from main form with null check
            if(ObjectClear != null)
            {
                // Invoke the object clear method
                ObjectClear(this, new EventArgs());
            }
        }

        // -----------------------------------------------------------------------------
        // Methods
        // -----------------------------------------------------------------------------
        // Hide the second form method
        public void HideWindow()
        {
            // Hide the second form when the 
            // display check mark is not visable
            Hide();
        }

        

        
    }
}
