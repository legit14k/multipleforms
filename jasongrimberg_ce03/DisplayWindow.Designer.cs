﻿namespace JasonGrimberg_CE03
{
    partial class DisplayWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tsClear = new System.Windows.Forms.ToolStripButton();
            this.tsDisplayWindow = new System.Windows.Forms.ToolStrip();
            this.lbDisplayWindow = new System.Windows.Forms.ListView();
            this.tsDisplayWindow.SuspendLayout();
            this.SuspendLayout();
            // 
            // tsClear
            // 
            this.tsClear.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsClear.Name = "tsClear";
            this.tsClear.Size = new System.Drawing.Size(55, 29);
            this.tsClear.Text = "Clear";
            this.tsClear.Click += new System.EventHandler(this.tsClear_Click);
            // 
            // tsDisplayWindow
            // 
            this.tsDisplayWindow.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.tsDisplayWindow.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsClear});
            this.tsDisplayWindow.Location = new System.Drawing.Point(0, 0);
            this.tsDisplayWindow.Name = "tsDisplayWindow";
            this.tsDisplayWindow.Padding = new System.Windows.Forms.Padding(0, 0, 2, 0);
            this.tsDisplayWindow.Size = new System.Drawing.Size(478, 32);
            this.tsDisplayWindow.TabIndex = 2;
            this.tsDisplayWindow.Text = "Tool Strip";
            // 
            // lbDisplayWindow
            // 
            this.lbDisplayWindow.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbDisplayWindow.Location = new System.Drawing.Point(0, 32);
            this.lbDisplayWindow.MultiSelect = false;
            this.lbDisplayWindow.Name = "lbDisplayWindow";
            this.lbDisplayWindow.Size = new System.Drawing.Size(478, 642);
            this.lbDisplayWindow.TabIndex = 3;
            this.lbDisplayWindow.UseCompatibleStateImageBehavior = false;
            this.lbDisplayWindow.View = System.Windows.Forms.View.List;
            this.lbDisplayWindow.SelectedIndexChanged += new System.EventHandler(this.lbDisplayWindow_SelectedIndexChanged);
            // 
            // DisplayWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(478, 674);
            this.Controls.Add(this.lbDisplayWindow);
            this.Controls.Add(this.tsDisplayWindow);
            this.MaximizeBox = false;
            this.Name = "DisplayWindow";
            this.ShowIcon = false;
            this.Text = "Display";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.DisplayWindow_FormClosed);
            this.Load += new System.EventHandler(this.DisplayWindow_Load);
            this.tsDisplayWindow.ResumeLayout(false);
            this.tsDisplayWindow.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.ToolStripButton tsClear;
        private System.Windows.Forms.ToolStrip tsDisplayWindow;
        private System.Windows.Forms.ListView lbDisplayWindow;
    }
}