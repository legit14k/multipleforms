﻿using System;
using System.Windows.Forms;
/// <summary>
/// Jason Grimberg
/// CE03: Custom Events
/// Visual Frameworks
/// </summary>
namespace JasonGrimberg_CE03
{
    public partial class MainForm : Form
    {
        DisplayWindow dwf = new DisplayWindow();

        // -----------------------------------------------------------------------------
        // EventHandler call
        // -----------------------------------------------------------------------------
        public EventHandler ObjectAdded;
        public EventHandler ObjectMainAdded;
        public EventHandler SelectionMainChanged;
        
        // -----------------------------------------------------------------------------
        // Student data components
        // -----------------------------------------------------------------------------
        // New student data
        public Students Data
        {
            // Get all of the student data
            get
            {
                Students s = new Students();
                s.StudentName = txtName.Text;
                s.StudentAge = numAge.Value;
                s.StudentStatus = cmboStudent.Text;
                s.StatusIndex = cmboStudent.SelectedIndex;
                s.StudentMale = rbMale.Checked;
                s.StudentFemale = rbFemale.Checked;
                return s;
            }

            // Set all of the student data
            set
            {
                txtName.Text = value.StudentName;
                numAge.Value = value.StudentAge;
                cmboStudent.Text = value.StudentStatus;
                rbMale.Checked = value.StudentMale;
                rbFemale.Checked = value.StudentFemale;
            }
        }

        // New Students selected object
        public Students SelectedMainObject
        {
            // Get what the user has selected
            get
            {
                // Make sure that the user has selected an object
                if (lbUsersMainView.SelectedItems.Count > 0)
                {
                    // Only return the first selected object
                    return lbUsersMainView.SelectedItems[0].Tag as Students;
                }
                else
                {
                    // Return nothing when the user has not selected anything
                    return new Students();
                }
            }
        }

        // -----------------------------------------------------------------------------
        // Initialize MainForm
        // -----------------------------------------------------------------------------
        public MainForm()
        {
            InitializeComponent();
        }

        // -----------------------------------------------------------------------------
        // Triggers
        // -----------------------------------------------------------------------------
        private void MainForm_Load(object sender, EventArgs e)
        {
            // Sub to the added handler on second form
            ObjectAdded += dwf.ObjectAddedHandler;

            // Sub to the mainForm handler for main list
            ObjectMainAdded += ObjectAddedHandler;

            // Sub to the selection change handler on the second form
            dwf.SelectionChanged += SelectionChangedHandler;

            // Sub to the selection change handler on the main form
            SelectionMainChanged += SelectionMainChangedHandler;
        }

        // Add to the lists
        private void btnAdd_Click(object sender, EventArgs e)
        {
            // Call add to list method
            AddToList();
        }

        // Clear all button on main form
        private void btnClearAll_Click(object sender, EventArgs e)
        {
            // Clear the main list 
            lbUsersMainView.Clear();

            // Call clear all method
            ClearAll();
        }

        // Exit menu button to exit
        private void exitMenu_Click(object sender, EventArgs e)
        {
            // Exit out of the application
            Application.Exit();
        }

        // Clear menu button to clear all objects
        private void menuClear_Click(object sender, EventArgs e)
        {
            // Clear main list
            lbUsersMainView.Clear();

            // Call clear all method
            ClearAll();
        }

        // Display menu button to pull up the second display form
        private void menuDisplay_Click(object sender, EventArgs e)
        {
            // If the display is checked then it will do nothing
            if(menuDisplay.Checked == true)
            {
                // Showing the second form with data
                dwf.Show();
            }
        }
        
        // -----------------------------------------------------------------------------
        // Object Handlers
        // -----------------------------------------------------------------------------
        // Clear object handler
        public void ObjectClearHandler(object sender, EventArgs e)
        {
            // Clearing the data from the screen with method
            Data = new Students();
        }

        // Object added handler
        public void ObjectAddedHandler(object sender, EventArgs e)
        {
            // Setting the sender as the main form
            MainForm mainForm = sender as MainForm;

            // Setting the students as the Data
            Students s = mainForm.Data;

            // Calling the list as a new list
            ListViewItem lvi = new ListViewItem();

            // Setting the items to string
            lvi.Text = s.ToString();

            // Setting the Tag as the main data
            lvi.Tag = s;

            // Adding the main data from the screen to the list
            lbUsersMainView.Items.Add(lvi);
        }

        // Selection changed handler
        private void SelectionChangedHandler(object sender, EventArgs e)
        {
            // Setting the selection from second screen to the data that is on the screen
            Data = dwf.SelectedObject;
        }

        // Selection main changed handler
        private void SelectionMainChangedHandler(object sender, EventArgs e)
        {
            // Setting the selection to the data that is selected
            Data = SelectedMainObject;
        }

        // -----------------------------------------------------------------------------
        // Change Events
        // -----------------------------------------------------------------------------
        // Change event when the display menu button is triggered
        private void menuDisplay_CheckedChanged(object sender, EventArgs e)
        {
            // Once the check is not there it will hide the second screen
            if (menuDisplay.Checked != true)
            {
                // Calling the method to hide the window
                dwf.HideWindow();
            }
        }

        // Main view selection changed
        private void lbUsersMainView_SelectedIndexChanged(object sender, EventArgs e)
        {
            // Invoke the selection changed with a null check
            if (SelectionMainChanged != null)
            {                
                SelectionMainChanged(this, new EventArgs());
            }
        }

        // -----------------------------------------------------------------------------
        // Methods
        // -----------------------------------------------------------------------------
        // Clear all method
        public void ClearAll()
        {
            // New data to clear all of the fields
            Data = new Students();
        }

        // Add to both lists method
        public void AddToList()
        {
            if(txtName.Text != "" && numAge.Value != 0 && cmboStudent.SelectedIndex != -1)
            {
                // Object added to second form with null check
                if (ObjectAdded != null)
                {
                    // Invoke the object added method
                    ObjectAdded(this, new EventArgs());
                }

                // Object added to the main form with null check
                if (ObjectMainAdded != null)
                {
                    // Invoke the object added main method
                    ObjectMainAdded(this, new EventArgs());
                }

                // Clear all of the inputs after adding
                ClearAll();
            }
            else
            {
                MessageBox.Show("Please fill out all information.", "Doh!", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            
        }

        
    }
}
